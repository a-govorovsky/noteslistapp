package com.personal.todo;

import java.util.Calendar;

import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.inject.Inject;
import com.personal.todo.dao.NotesDAO;
import com.personal.todo.helper.database.ServiceConstantHelper;
import com.personal.todo.model.Note;

public class EditEventActivity extends RoboActivity implements View.OnClickListener{

	@Inject
	private NotesDAO notesDAO;
	
	@InjectView(R.id.name)
	TextView noteName;
	
	@InjectView(R.id.description)
	TextView description;
	
	@InjectView(R.id.editNote)
	Button save;
	
	private Note editedNote;
	
	private String noteID;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_event);
        Bundle extras = getIntent().getExtras();
        if(null == extras)
        	return;
        
        noteID = (String)extras.get(ServiceConstantHelper.NOTE_ID);
        Note noteById = notesDAO.getNoteByID(Integer.valueOf(noteID));
        noteName.setText((CharSequence)noteById.getName());
        description.setText((CharSequence)noteById.getDescription());
        save.setOnClickListener(this);
        
    }

    @Override
	public void onClick(View v) {
		Note note = new Note();
		note.setId(Integer.valueOf(noteID));
		note.setName(noteName.getText().toString());
		note.setDescription(description.getText().toString());
		note.setDateLastModified(Calendar.getInstance().getTime());
		editedNote = notesDAO.editNote(note);
		finish();
		
	}
    @Override
    public void finish() {
    	Intent intent =  new Intent();
		intent.putExtra(ServiceConstantHelper.NOTE_EDIT, this.editedNote);
		setResult(ServiceConstantHelper.EDIT_NOTE, intent);
		super.finish();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_edit_event, menu);
        return true;
    }
}
