package com.personal.todo.modules;

import android.widget.ArrayAdapter;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import com.google.inject.TypeLiteral;
import com.personal.todo.adapter.CustomNoteListAdapter;
import com.personal.todo.dao.NotesDAO;
import com.personal.todo.dao.NotesDAOImpl;
import com.personal.todo.helper.database.MySQLHelper;
import com.personal.todo.model.Note;

public class ApplicationModule extends AbstractModule{

	public ApplicationModule() {

	}
	@Override
	protected void configure() {
		bind(MySQLHelper.class).in(Scopes.SINGLETON);
		bind(new TypeLiteral<ArrayAdapter<Note>>(){}).to(CustomNoteListAdapter.class);
		bind(NotesDAO.class).to(NotesDAOImpl.class);
	}
	
}
