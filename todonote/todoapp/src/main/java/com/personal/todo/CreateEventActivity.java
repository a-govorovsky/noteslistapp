package com.personal.todo;

import java.util.Calendar;

import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.inject.Inject;
import com.personal.todo.dao.NotesDAO;
import com.personal.todo.helper.database.ServiceConstantHelper;
import com.personal.todo.model.Note;

public class CreateEventActivity extends RoboActivity implements View.OnClickListener{

	private Note newNote;
	
	@Inject
	private NotesDAO notesDao;
	
	@InjectView(R.id.createNote)
	Button create;
	
	@InjectView(R.id.name)
	TextView name;
	
	@InjectView(R.id.description)
	TextView description;
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event);
        create.setOnClickListener(this);
    }

    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_create_event, menu);
        return true;
    }


	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.createNote)
		{
			String noteName = name.getText().toString();
			
			String noteDescription = description.getText().toString();
			Note note = new Note();
			note.setName(noteName);
			note.setDescription(noteDescription);
			note.setDateCreated(Calendar.getInstance().getTime());
			note.setDateLastModified(Calendar.getInstance().getTime());
			this.newNote = notesDao.addNote(note);
			Log.i(CreateEventActivity.class.toString(), newNote.toString());
			finish();
		}
		
	}
	
	@Override
	public void finish() {
		Intent intent =  new Intent();
		intent.putExtra(ServiceConstantHelper.NOTE_CREATE, this.newNote);
		setResult(ServiceConstantHelper.CREATE_NOTE, intent);
		super.finish();
	}
}

