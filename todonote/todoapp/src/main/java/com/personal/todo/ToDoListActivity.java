package com.personal.todo;

import java.util.ArrayList;
import java.util.List;

import roboguice.inject.InjectView;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.ActionMode.Callback;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.github.rtyley.android.sherlock.roboguice.activity.RoboSherlockListActivity;
import com.google.inject.Inject;
import com.personal.todo.dao.NotesDAO;
import com.personal.todo.helper.database.ServiceConstantHelper;
import com.personal.todo.model.Note;

public class ToDoListActivity extends RoboSherlockListActivity {

	@InjectView(android.R.id.list)
	protected ListView viewList;

	@Inject
	private NotesDAO notesDao;

	@Inject
	private ArrayAdapter<Note> adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_to_do_list);
		List<Note> notesList = new ArrayList<Note>();

		// check for the search option is excercised
		// if (getIntent().getAction().equals(Intent.ACTION_SEARCH)) {
		// String query = getIntent().getStringExtra(SearchManager.QUERY);
		// Log.e(ToDoListActivity.class.toString(), "search string is:"
		// + query);
		// notesList = notesDao.searchNoteByName(query);
		// }
		// else
		// {
		notesList = notesDao.getAllNotes();
		// }

		adapter.addAll(notesList);
		adapter.sort(new Note.NameComparator());

		setListAdapter(adapter);
		registerForContextMenu(viewList);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemId = item.getItemId();
		switch (itemId) {
		case R.id.createNote:
			createNewNote();
			return true;
		default:
			break;
		}

		return false;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getSupportMenuInflater().inflate(R.menu.activity_to_do_list, menu);
		SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		SearchView searchWidget = (SearchView) menu.findItem(R.id.searchNotes)
				.getActionView();
		searchWidget.setSubmitButtonEnabled(true);
		searchWidget.setSearchableInfo(manager
				.getSearchableInfo(new ComponentName(getApplicationContext(),
						SearchActivity.class)));

		return true;
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		String notesID = ((TextView) v.findViewById(R.id.noteID)).getText()
				.toString();
		startActionMode(new ActionModeCallback(this, notesID));
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Bundle bundle = data.getExtras();
		if (resultCode == ServiceConstantHelper.CREATE_NOTE) {
			Note newNote = (Note) bundle.get(ServiceConstantHelper.NOTE_CREATE);
			if (null == newNote)
				return;
			adapter.add(newNote);
			adapter.sort(new Note.NameComparator());
			setListAdapter(adapter);
		}
		if (resultCode == ServiceConstantHelper.EDIT_NOTE) {

			Note editedNote = (Note) bundle
					.get(ServiceConstantHelper.NOTE_EDIT);
			if (null == editedNote)
				return;
			adapter.remove(editedNote);
			adapter.add(editedNote);
			adapter.sort(new Note.NameComparator());
			setListAdapter(adapter);
		}
	}

	private void remove(int notesID) {
		Note noteByID = notesDao.getNoteByID(notesID);
		if (null != noteByID) {
			if (notesDao.removeNoteById(notesID)) {
				adapter.remove(noteByID);
			}
		}
	}

	private void editNote(int noteID) {
		Intent editIntent = new Intent(this, EditEventActivity.class);
		editIntent.putExtra(ServiceConstantHelper.NOTE_ID,
				String.valueOf(noteID));
		this.startActivityForResult(editIntent, ServiceConstantHelper.EDIT_NOTE);
	}

	private void createNewNote() {
		Intent createNewIntent = new Intent(this, CreateEventActivity.class);
		this.startActivityForResult(createNewIntent,
				ServiceConstantHelper.CREATE_NOTE);
	}

	class ActionModeCallback implements Callback {

		private ToDoListActivity toDoList;

		private String notesID;

		public ActionModeCallback(ToDoListActivity activity, String notesID) {
			toDoList = activity;
			this.notesID = notesID;

		}

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			menu.clear();
			mode.getMenuInflater().inflate(R.menu.menu_list_options, menu);
			return true;
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			int id = item.getItemId();
			switch (id) {
			case R.id.editNote:
				toDoList.editNote(Integer.valueOf(notesID));
				return true;
			case R.id.deleteNote:
				toDoList.remove(Integer.valueOf(notesID));
				return true;
			}
			mode.finish();
			return false;
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			notesID = null;
		}
	}
}
