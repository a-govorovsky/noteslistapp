package com.personal.todo.events;

import android.app.Activity;
import android.content.Intent;

import com.personal.todo.EditEventActivity;
import com.personal.todo.helper.database.ServiceConstantHelper;

public class EditEvent {
	private Activity context;

	public EditEvent(Activity context) {
		this.context = context;
	}

	private void editEvent(String noteID) {
		Intent editIntent = new Intent(context, EditEventActivity.class);
		editIntent.putExtra(ServiceConstantHelper.NOTE_ID, noteID);
		context.startActivityForResult(editIntent,
				ServiceConstantHelper.EDIT_NOTE);
	}

	public void fireEditEvent(String noteID) {
		editEvent(noteID);
	}
}
