package com.personal.todo;

import roboguice.inject.InjectView;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.ListView;
import android.widget.SearchView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.github.rtyley.android.sherlock.roboguice.activity.RoboSherlockFragmentActivity;

public class SearchActivity extends RoboSherlockFragmentActivity implements
		ActionBar.TabListener {

	@InjectView(android.R.id.list)
	ListView listView;
	
	private String searchString;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_activity_layout);
		handleSearch();
		ActionBar supportActionBar = getSupportActionBar();
		supportActionBar.setDisplayHomeAsUpEnabled(true);
		supportActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		Tab newTab = supportActionBar.newTab();
		newTab.setText("Name");
		newTab.setTabListener(this);
		Tab newTab1 = supportActionBar.newTab();
		newTab1.setText("Description");
		newTab1.setTabListener(this);
		supportActionBar.addTab(newTab);
		supportActionBar.addTab(newTab1);

	}

	private void handleSearch() {
		if (getIntent().getAction().equals(Intent.ACTION_SEARCH)) {
			searchString = getIntent().getStringExtra(SearchManager.QUERY);
			Log.d(SearchActivity.class.toString(), "Search String is:" + searchString);
		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		// TODO Auto-generated method stub
		setIntent(intent);
		handleSearch();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getSupportMenuInflater().inflate(R.menu.activity_to_do_list, menu);
		
		SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		menu.findItem(R.id.searchNotes).expandActionView();
		
		SearchView searchWidget = (SearchView) menu.findItem(R.id.searchNotes)
				.getActionView();
		searchWidget.setSubmitButtonEnabled(true);
		searchWidget.setSearchableInfo(manager
				.getSearchableInfo(getComponentName()));
		searchWidget.setIconifiedByDefault(false);
		
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemID = item.getItemId();
		switch (itemID) {
		case android.R.id.home:
			finish();
			return true;
		default:
			break;
		}
		return false;
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}
}
