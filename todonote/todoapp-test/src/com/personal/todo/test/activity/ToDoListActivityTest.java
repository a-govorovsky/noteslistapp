package com.personal.todo.test.activity;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.personal.todo.ToDoListActivity;
import com.xtremelabs.robolectric.RobolectricTestRunner;


@RunWith(RobolectricTestRunner.class)
public class ToDoListActivityTest extends TestCase {

	private ToDoListActivity toDoActivity ;
	
	@Before
	public void setup()
	{
		toDoActivity = new ToDoListActivity();
		toDoActivity.onCreate(null);
	}
	
	@Test
	public void activityNotNull()
	{
		assertNotNull(toDoActivity);
	}
}
